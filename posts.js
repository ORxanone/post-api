const express = require("express");
const router = new express.Router();
const uniqid = require("uniqid");


let posts = [];

router.get("/posts", (req, res) => {
  res.status(200).send(posts);
});

router.post("/posts", (req, res) => {
  const { title, content } = req.body;
  const newPost = {
    id: uniqid(),
    title,
    content,
    userId: req.user.id,
  };
  posts.push(newPost);
  res.status(200).send(newPost);
});

router.get("/posts/:id", (req, res) => {
  const post = posts.find((post) => post.id === req.params.id);
  if (post) {
    res.status(200).send(post);
  } else res.status(404).send({ message: "Post Not Found" });
});

router.put("/posts/:id", (req, res) => {
  const post = posts.find((post) => post.id === req.params.id);
  const { title, content } = req.body
  if(post){
    if(post.userId === req.user.id){
        post.title = title
        post.content = content
        res.status(204).send(post)
    }else{
        res.status(403).send({
            message: "You are not authorized to edit this post"
        })
    }
  }else{
    res.status(404).send({ message: "Post Not Found" })
  }

});

router.delete("/posts/:id", (req, res) => {
  const index = posts.findIndex((post) => post.id === req.params.id);
  if(index !== -1){
      posts.splice(index, 1);
    res.status(204).send()
  }else{
    res.status(404).send({ message: "Post Not Found" });
  }
    // posts = posts.filter((post)=> post.id !== req.params.id)
});

module.exports = router