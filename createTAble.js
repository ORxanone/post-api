
const client = require("./database");

client.query(`
create table users(
    id SERIAL PRIMARY KEY
    ,user_id INT
    ,fullname VARCHAR(50)
    ,surname VARCHAR(50)
    ,username VARCHAR(50)	
    ,salt     VARCHAR(100)	     
    ,password VARCHAR(255)       
    ,status   int             
    ,created_at  TIMESTAMP DEFAULT NOW()
    ,updated_at  TIMESTAMP DEFAULT NOW()
    ,parent      INT
    ,profileimage VARCHAR(255)
);

CREATE TABLE activ_user_filter 
(
	id SERIAL PRIMARY KEY
    ,user_id INTEGER REFERENCES users(id) on delete cascade
	,ins_date TIMESTAMP DEFAULT NOW()
);
`,
  (err, res) => {
    if (err) {
      console.log(err.message);
      return;
    }
    console.log(res.rows);
    client.end();
  }
);
