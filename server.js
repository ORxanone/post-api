const express = require("express");
const postRoutes = require('./posts')
const userRoutes = require('./user')
const jwt = require('jsonwebtoken')

const app = express();

app.use((req, res, next)=>{
    if(req.url === '/registration' || req.url === '/login'){
        next();
        return;
    }
    const accessToken = req.headers['authorization']
    if(accessToken){
        jwt.verify(accessToken, '12345', (err, decoded)=>{
            if(err){
                res.status(401).send({message: 'Invalid Token'})
            }else{
                req.user = decoded
                next()
            }
        })
    }else{
        res.status(401).send({message: 'Unauthorized request'})
    }
})

app.use(express.json());
app.use(postRoutes);
app.use(userRoutes);



app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
