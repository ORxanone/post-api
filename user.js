const express = require('express')
const crypto = require('crypto')
const uniqid = require('uniqid')
const router = express.Router()
const jwt = require('jsonwebtoken')
const client = require('./database')

const SALT = '12345';
const SECRET_KEY = '12345';



router.post('/registration', async(req, res)=>{
    const { username, fullName, password, surname } = req.body

    const query = `SELECT id FROM users WHERE username='${username}'`
    const result =  await client.query(query)

    if(result.rowCount > 0){
        res.status(400).send({
            message: 'Username Already exist'
        })
        return;
    }
    const hashedPassword = crypto.pbkdf2Sync(password, 'salt', 10000, 64, 'sha512').toString('hex')

    await client.query(`INSERT INTO users (username, password, fullname, surname)
    VALUES ('${username}', '${hashedPassword}', '${fullName}', '${surname}')`)

    res.status(201).send()
})

router.post('/login', async(req, res)=>{
    const { username, password } = req.body
    const hashedPassword = crypto.pbkdf2Sync(password, 'salt', 10000, 64, 'sha512').toString('hex')
    // const user = users.find(user => user.username === username && user.password === hashedPassword);
    const result = await client.query(`SELECT * FROM users WHERE username ='${username}' AND password = '${hashedPassword}'`)
    console.log(result.rows)
    if(result.rowCount > 0){
        const { password, ...theRest } = result.rows[0]
        const accessToken = jwt.sign(theRest, SECRET_KEY)
        res.status(200).send({
            accessToken,
        })
    }else res.status(401).send({message: 'Username and password is wrong'});
})


module.exports = router