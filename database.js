const { Client } = require("pg");

const client = new Client({
  host: "localhost",
  user: "postgres",
  password: "mysecretpassword ",
  database: "postgres",
  port: 5432,
});

client
	.connect()
	.then(() => {
		console.log('Connected to PostgreSQL database');
	})
	.catch((err) => {
		console.error('Error connecting to PostgreSQL database', err);
	});


module.exports = client
